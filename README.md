# wbBase

This is the base package for WorkBench applications.
Workbench is a plugin based RAD framework for platform independent GUI applications
based on [wxPython](https://pypi.org/project/wxPython/).

## Installation

```shell
pip install wbBase
```

## Documentation

For details read the [Documentation](https://workbench2.gitlab.io/wbbase/).

## Plugins
The following plugins are currently available as individual packages.

- [wbpOutput](https://pypi.org/project/wbpoutput/), 
    an output panel for Workbench applications.
- [wbpShell](https://pypi.org/project/wbpshell/), 
    a shell panel for Workbench applications.
- [wbpLoglist](https://pypi.org/project/wbploglist/),
    a log list panel for Workbench applications.
- [wbpNamespace](https://pypi.org/project/wbpnamespace/),
    a namespace panel for Workbench applications.
- [wbpWidgetinspector](https://pypi.org/project/wbpwidgetinspector/)
    integrates the Widgetinspector in Workbench applications.
- [wbpFilebrowser](https://pypi.org/project/wbpfilebrowser/),
    a file browser panel for Workbench applications.
- [wbpTextedit](https://pypi.org/project/wbptextedit/),
    document templates to create, view and edit some text file types.
- [wbpUItools](https://pypi.org/project/wbpuitools/),
    collection of some useful functions for user interaction in Python scripts.
- [wbpHTMLpanel](https://pypi.org/project/wbpHTMLpanel/),
    a panel to display arbitrary html.
