wbBase.dialog
=====================

.. automodule:: wbBase.dialog
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 2

   multiSaveModifiedDialog
   multiSaveModifiedDialogUI
   preferences
