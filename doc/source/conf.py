# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
import wbBase
sys.path.insert(0, os.path.abspath('../../Lib/wbBase'))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'wbBase'
copyright = '2023, Andreas Eigendorf'
author = 'Andreas Eigendorf'
release = wbBase.__version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    'sphinx.ext.viewcode',
    # 'autodocsumm',
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_show_sourcelink = False
html_show_sphinx = False
html_title = "wbBase"
html_static_path = ['_static']

add_module_names = False
autodoc_member_order = "bysource"
autodoc_typehints = "both"
autodoc_typehints_format = "short"
autodoc_default_flags = [
    "members",
    "inherited-members",
    "show-inheritance",
]
autodoc_class_signature = "separated"
autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "inherited-members": False,
    'autosummary': True,
}
