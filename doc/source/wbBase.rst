wbBase API documentation
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 2

   control
   dialog
   document

Submodules
----------

.. toctree::
   :maxdepth: 2

   application
   applicationWindow
   artprovider
   panelManager
   pluginManager
   scripting
   tools
