wbBase.document
=======================

.. automodule:: wbBase.document
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 2

   manager
   notebook
   template
   view
