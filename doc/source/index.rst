.. wbBase documentation master file, created by
   sphinx-quickstart on Wed Jan  4 13:09:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wbBase's documentation!
==================================

wbBase is the basic package of the Workbench RAD framework
for platform independant GUI applications.


.. figure:: _static/workbench.png
   :scale: 100 %
   :alt: Workbench screenshot


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wbBase

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
