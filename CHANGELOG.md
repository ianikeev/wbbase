# wbBase Changelog

## Release [0.1.47] not released yet

Added class variables to ApplicationWindow for type of 

- panelManager
- pluginManager
- documentManager

... to make subclassing easier.

Added class variable to App for type of TopWindow for the same reason.

## Release [0.1.46](https://gitlab.com/workbench2/wbbase/-/releases/0.1.46)

setup.cfg fixed to show long description on PyPI


## Release [0.1.45](https://gitlab.com/workbench2/wbbase/-/releases/0.1.45)

First public release on PyPI
