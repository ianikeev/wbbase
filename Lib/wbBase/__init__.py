import logging

from .application import App
from .applicationWindow import ApplicationWindow

__version__ = "0.2.9"

log = logging.getLogger(__name__)
